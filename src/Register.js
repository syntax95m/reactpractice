import React from 'react';
import '../public/style.css';
import { Link } from 'react-router-dom';
import { PostData } from '../public/PostData.js';
import { Redirect } from 'react-router-dom';

const Register = React.createClass({
    getInitialState: function() {
        return {
          username: '',
          password: '',
          email: '',
          redirectFlag: false // As it's initially not logged-in, redirection to "Landing" is allowed
        };
      },
      login: function() {
        if(this.state.username && this.state.password) {
          PostData('register', this.state).then((result) => { // Pass the current state to the PostData "fetch function" at /public/PostData.js
            let responseJSON = result;
            if(responseJSON.userId) { // Validates that the responseJSON has been successfully arrived
              sessionStorage.setItem('userData', responseJSON); // Stores the user data into session storage
              this.setState({ redirectFlag: true }); // To prevent redirection to "Landing/Login" while logged-in
            }
            else {
              alert("Sorry, incorrect data or user not found!");
            }
          });
        }
        else {
          alert("Please, enter your username and password to login!")
        }
    },
    onInputChange: function(evt) {
        this.setState({ [evt.target.name]: evt.target.value }); // evt.target.name -> to specify which input we are in, and then change its state
        console.log(this.state);
    },
    render: function() {
        if(sessionStorage.getItem("userData")) { // If session storage has "userData"..
            return <Redirect  to={'/search'} />; // Automatically redirect to Search Component
        }
        if(this.state.redirectFlag) { // If redirectFlag is true ..
            return <Redirect to={'/search'} /> // Redirect to Search Component when trying to access "Landing"
        }
        return (
            <div className='landing'>
                <h1>New Account</h1>
                <input type='text' className="username" name="username" placeholder='Insert username ..' onChange={this.onInputChange} />
                <input type='password' className="password" name="password" placeholder='Insert password ..' onChange={this.onInputChange} />
                <input type='email' className="email" name="email" placeholder='Insert email ..' onChange={this.onInputChange} />
                <button className="login-button-style" onClick={this.signup}>Sign-In</button>
                {/*<Link on to='/search'>Sign-In Now</Link>*/}
            </div>
        );
    }
});

export default Register;