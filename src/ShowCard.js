import React from 'react';

/* Props Types */
const { string } = React.PropTypes; // Destructuring

const ShowCard = React.createClass({
  /* Specifing Props Types */
  propTypes: {
    // Expecting an object from my parent called "singleShow" and the shape of it is...
    poster: string,
    title: string,
    year: string,
    description: string
  },
  render: function () {
    return (
      <div className='show-card'>
        <div className='poster-image'>
          <img src={`/public/img/posters/${this.props.poster}`} />
        </div>
        <div className="details-info">
          <h3>{this.props.title}</h3>
          <h3>({this.props.year})</h3>
          <p>{this.props.description}</p>
        </div>
      </div>
    );
  }
});

export default ShowCard;
