import React from 'react';

const Details = React.createClass({
    render: function() {
        return (
            <div className="details">
                {JSON.stringify(this.props, null, 4)}
            </div>
        );
    }
});

export default Details;