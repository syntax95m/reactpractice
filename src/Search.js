import React from 'react';
import { Link } from 'react-router-dom';
import ShowCard from './ShowCard';
import { Redirect } from 'react-router-dom';
import Landing from './Landing';
import LoginToView from './LoginToView';
import Logout from './Logout';

const Search = React.createClass({
 
  getInitialState: function () {
    return {
      searchTerm: ''
    };
  },
  handleSearchTermState: function (evt) {
    this.setState({ searchTerm: evt.target.value });
  },
  logout: function() { // Logout handler function
    let confirmLogout = confirm("Are you sure you'd like to logout?"); // Popup warning
    if(confirmLogout === true) {
      sessionStorage.setItem("userData", ''); // Empty the userData from session storage
      sessionStorage.clear(); // clear session storage
    }
  },
  render: function () {

    /* We're in the Search page right now, and, as we don't want this page to be displayed globally unless the user in logged-in,
    I checked first "if the user data are not in the session storage", "then, redirect him to (Please login) Component" */
    if(!(sessionStorage.getItem("userData"))) {
      return <Redirect  to={'/LoginToView'} />;
    }

    /* Else.. If found data in storage, display normal Search. */
    return (
      <div className='search'>
        <header>
          <h1>Reapp - React.js Applications</h1>
          <input onChange={this.handleSearchTermState} type='text' placeholder='Search..' value={this.state.searchTerm} />
          
          <Link to="/logout" onClick={this.logout}> {/* Log-out functionality, if clicked "logout", warn him, then, redirect and clear storage session*/}
            Logout
          </Link>
        </header>
        <div>
          { this.props.shows.filter(
            singleShow => `${singleShow.title} ${singleShow.description}`.toUpperCase().indexOf(this.state.searchTerm.toUpperCase()) >= 0
          )
            .map(function (singleShow) {
              return (
                <ShowCard
                  key={singleShow.imdbID}
                  poster={singleShow.poster}
                  title={singleShow.title}
                  year={singleShow.year}
                  description={singleShow.description} />
              );
            }) }
        </div>
        <Link to='/'>Go Back</Link>
      </div>
    );
  }
});

export default Search;
