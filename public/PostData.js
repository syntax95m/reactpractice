export function PostData(type, userData) { // "type" parameter is used that we might use the function in different cases "login", "signup" ..etc
    return new Promise((resolve, reject) => {
        fetch('http://159.65.239.49:3000/api/Users/' + type, { // Our LoopBack API
            method: 'POST', // "POST"
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(userData) // Transform the userData to JSON
        })
        .then((response) => response.json())
        .then((responseJson) => {
            resolve(responseJson);
        })
        .catch((error) => {
            reject(error);
        });
    });
}